import { BaseUnit } from "./app/baseUnit";

export const HEROES: BaseUnit[] = [
    {   
        id: 1, 
        name: "Jaapie",
        description: "New Hero",
        prortait: null,
        movementType: "armored",
        weaponType: "sword",
        statHp: 30,
        statAtk: 50,
        statSpd: 50,
        statDef: 45,
        statRes: 45,
        superBoon: "atk,spd",
        superBane: "hp"
    },
    {   
        id: 2, 
        name: "Kerel",
        description: "Dark Mon",
        prortait: null,
        movementType: "infantry",
        weaponType: "lance",
        statHp: 30,
        statAtk: 50,
        statSpd: 50,
        statDef: 45,
        statRes: 45,
        superBoon: "hp,spd",
        superBane: "def,res"
    },
    {   
        id: 3, 
        name: "Freek",
        description: "Big Bad",
        prortait: null,
        movementType: "cavalier",
        weaponType: "dagger",
        statHp: 30,
        statAtk: 50,
        statSpd: 50,
        statDef: 45,
        statRes: 45,
        superBoon: "atk,spd,def",
        superBane: "hp,atk,res"
    }

]