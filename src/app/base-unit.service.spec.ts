import { TestBed } from '@angular/core/testing';

import { BaseUnitService } from './base-unit.service';

describe('BaseUnitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BaseUnitService = TestBed.get(BaseUnitService);
    expect(service).toBeTruthy();
  });
});
