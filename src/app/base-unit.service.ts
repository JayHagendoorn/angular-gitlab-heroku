import { Injectable } from '@angular/core';
import { BaseUnit } from './baseUnit';
import { HEROES } from '../mock-baseUnit';

@Injectable({
  providedIn: 'root'
})
export class BaseUnitService {

  getBaseUnits(): BaseUnit[] {
    return HEROES;
  }
  
  constructor() { }
}
