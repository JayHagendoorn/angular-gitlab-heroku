import { Component, OnInit } from '@angular/core'
import { UseCase } from '../usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  title = 'Feh Unit Tracker'
  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'De gebruiker kan inloggen met zijn/haar account.',
      scenario: [
        'De gebruiker vult zijn/haar gebruikersnaam en wachtwoord in en klikt op Login knop.',
        'De applicatie checkt de gegeves voor validatie.',
        'De applicatie stuurt de gebruiker verder als de gegevens kloppen.'
      ],
      actor: 'Gebruiker met een account',
      precondition: 'De gebruiker heeft een account hebben',
      postcondition: 'De gebruiker is ingelogd'
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'De gebruiker kan een account aanmaken maken.',
      scenario: [
        'De gebruiker vult zijn/haar gewenste gebruikersnaam en wachtwoord in en klikt op registreren knop.',
        'De applicatie checkt de gegevens voor validatie.',
        'De applicatie stuur de gebruiker verder als de gegevens kloppen.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker heeft geen account.',
      postcondition: 'De gebruiker heeft een account.'
    },
    {
      id: 'UC-03',
      name: 'Aanmaken nieuwe template unit',
      description: 'De gebruiker kan een template unit maken.',
      scenario: [
        'De gebruiker vult de naam, beschrijving, stats en types van de unit in en geeft een plaatje mee.',
        'De applicatie checkt de gegevens voor validatie.',
        'Als de unit niet bestaat met die naam en beschrijving wordt de unit toegevoegd.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'Een nieuwe template unit is toegevoegd.'
    },
    {
      id: 'UC-04',
      name: 'Aanmaken unit',
      description: 'De gebruiker kan een unit aanmaken maken op basis van een template unit.',
      scenario: [
        'De gebruiker kiest een template unit.',
        'De gebruiker vult de boon en bane in.',
        'De gebruiker kiest alle skills een favoriet nummer indien gewenst.',
        'De applicatie checkt de gegevens voor validatie.',
        'De applicatie voegt de unit toe en koppelt het aan het account van de gebruiker.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'De gebruiker heeft een nieuwe unit.'
    },
    {
      id: 'UC-05',
      name: 'Aanmaken skill',
      description: 'De gebruiker kan een skill aanmaken.',
      scenario: [
        'De gebruiker vult de naam en beschrijving in.',
        'De gebruiker voegt een afbeelding toe, geeft de restricties mee en kiest de skill die ervoor komt indien nodig',
        'De applicatie checkt de gegevens voor validatie.',
        'De applicatie voegt de skill toe.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'Een nieuwe skill is toegevoegd.'
    },
    {
      id: 'UC-06',
      name: 'Bekijken gebruikers units',
      description: 'De gebruiker kan de units bekijken die hij/zij heeft gemaakt.',
      scenario: [
        'De gebruiker bekijkt de zelfgemaakte units.',
        'De applicatie bekijkt de gebruiker.',
        'Indien de gebruiker units heeft gemaakt laat de applicatie zijn/haar units zien.'
      ],
      actor: 'Gebruiker',
      precondition: 'De gebruiker is ingelogd.',
      postcondition: 'De zelfgemaakte units verschijnen op het scherm.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
