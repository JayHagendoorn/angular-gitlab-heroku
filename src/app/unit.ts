export class Unit {
    id: number;
    baseUnitId: number;
    userId: number;
    boon: string;
    bane: string;
    favorite: number;
    skillWeapon: number;
    skillAssist: number;
    skillSpecial: number;
    skillA: number;
    skillB: number;
    skillC: number;
    skillS: number;
}