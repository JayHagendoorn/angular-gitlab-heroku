export class BaseUnit {
    id: number;
    name: string;
    description: string;
    prortait: File;
    movementType: string;
    weaponType: string;
    statHp: number;
    statAtk: number;
    statSpd: number;
    statDef: number;
    statRes: number;
    superBoon: string;
    superBane: string;
}